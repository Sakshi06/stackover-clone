<?php

namespace App\Http\Controllers;

use App\Http\Requests\Questions\CreateQuestionRequest;
use App\Http\Requests\Questions\UpdateQuestionRequest;
use App\Models\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class QuestionsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth'])->except('index', 'show');
    }
    public function index()
    {
        $questions = Question::with('owner')->latest()->paginate(10);
        return view('questions.index', compact(['questions']));
    }


    public function create()
    {
        app('debugbar')->disable();
        return view('questions.create');
    }


    public function store(Request $request)
    {
        auth()->user()->questions()->create([
            'title' => $request->title,
            'body'=>$request->body
        ]);
        session()->flash('success','Question has been added successfully!');
        return redirect(route('questions.index'));
    }


    public function show(Question $question)
    {
        $question->increment('views_count');
        return view('questions.show', compact(['question']));

    }

    public function edit(Question $question)
    {
        if($this->authorize('update', $question)) {
            return view('questions.edit', compact(['question']));
        }
        abort(403, 'Access Denied!');
    }


    public function update(UpdateQuestionRequest $request, Question $question)
    {
        if($this->authorize('update', $question)) {
            $question->update([
                'title' => $request->title,
                'body' => $request->body
            ]);

            session()->flash('success', 'Question has been updated successfully!');
            return redirect(route('questions.index'));
        }
        abort(403);
    }


    public function destroy(Question $question)
    {
        if($this->authorize('delete', $question)) {
            $question->delete();
            session()->flash('success', 'Question has been deleted successfully!');
            return redirect(route('questions.index'));
        }
        abort(403);
    }
}
